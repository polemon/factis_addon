# factis_addon

Small factis addon facilitating quick use of their web UI, using only output
parsing and HTML DOM traversal.

Specifically, day view calculates times based on working day sections, and consecutive jobs,
just from the rendered ouput. The results are also rendered in the same view, not necessitating
changes to the utility.
The table view is expanded by adding line-based highlighting and an adjusted vertical scrolling.

## installation

This plugin requires tampermonkey.

Load the scripts into tampermonkey and adjust the line beginning with `// @match` in both files:

*(Factis_day_view_addon.user.js)*
```
// @match        <ENDPOINT>/mitarbeiter*
```
and

*(Factis_table_view_addon.user.js)*
```
// @match        <ENDPOINT>/arbeitszeiten*
```

In those lines `<ENDPOINT>` needs to be changed to whatever your factis endpoint is,
which is usually the URL that you access when loging into your factis endpoint.

Example:
    your endpoint URL is *https://umi0.umifact.de/corp/zentral*, then that exact URL
    is your endpoint and shout go where `<ENDPOINT>` is in those aforementioned lines.

## controls

 * __In day view:__

   `w`: scroll up

   `s`: scroll down

   `a`: go one day back

   `d`: go one day ahead

   `q`: go one month back (aligned to 1st of every month)

   `e`: go one month ahead (aligned to 1st ot every month)

    additionally, day-section-wise calculations are made and displayed into a rendered table.
    consecutive jobs are highlighted.

 * __In table view:__

   `w`: scroll up (aligned to one table row height)

   `s`: scroll down (aligned to one table row height)

   `q`: go one month back

   `e`: go one month ahead

   additionally, moving the mouse over the table highlights the row over which the mouse pointer
   is hovering.
   Clicking into that row will mark that row highlighted. Clicking that row again, cancels that
   additional highlight, clicking another row will cancel the previous highlight.


 * __Small UI tweaks:__

   Additional movement buttons are placed into the top bar.
   Pull-down menus are sped-up by removing all of the visual effects associated with them.


## Note
**The day view addon calculation is only working correctly when the "compressed display"
option is ticked!**

Reading the already relatively ambiguous output and parsing it, is complicated as it is,
having extra information being displayed, complicates things only even more so.
    

