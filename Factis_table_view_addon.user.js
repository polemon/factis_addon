// ==UserScript==
// @name         Factis_table_view_addon
// @namespace    http://tampermonkey.net/
// @version      0.6
// @description  add functions and enahnce view of Factis saas
// @author       Szymon 'polemon' Bereziak
// @match        <ENDPOINT>/arbeitszeiten*
// @grant        GM_addStyle
// @require      https://code.jquery.com/jquery-3.6.0.min.js
// ==/UserScript==
// Last change   2021-11-08

// FIXME tooltips, etc?
// require      https://code.jquery.com/ui/1.13.0/jquery-ui.min.js

(function() {
    'use strict';

    GM_addStyle(`
        /* @import url('https://fonts.googleapis.com/css2?family=Share+Tech+Mono&display=swap'); */
        @import url('https://fonts.googleapis.com/css2?family=JetBrains+Mono&display=swap');

        .spacer:hover, .label:hover {
            background-color: inherit !important;
        }

        .spacer, .label {
            border-left: 0 !important;
            font-size: 18px !important;
            vertical-align: top;
        }

        #day_fwd, #mon_fwd {
            background-color: rgb(32 167 136);
            border-right: 1px solid #1a71a0;
        }

        #day_fwd:hover, #mon_fwd:hover {
            background-color: rgb(16 95 77) !important;
        }

        table.arbeitszeiten {
            border-collapse: separate;
        }

        table.arbeitszeiten td,
        table.arbeitszeiten th {
            border-right: 1px solid #e1e2e3;
            border-bottom: 1px solid #e1e2e3;
            border-left: 0;
            border-top: 0;
        }

        table.arbeitszeiten td:first-child,
        table.arbeitszeiten th:first-child {
            border-left: 1px solid #e1e2e3;
        }

        th.summe, th.tag,
        td.summe, td.tag {
            font-size: 11pt !important;
            font-family: 'Iosevka Nerd Font Mono', monospace !important;
            /* font-family: 'Share Tech Mono', monospace !important; */
            /* font-family: 'JetBrains Mono', monospace !important; */
            font-weight: 350;
            letter-spacing: -0.05ex !important;
        }

        td.name {
            font-size: 1.72ex !important;
            padding: 3px !important;
        }

        td.summe b {
            font-weight: 600;
        }

        td.leer {
            color: #eee;
        }

        th.wochenende.tag {
            background-color: #d1d2d3;
        }

        .static_highlight {
            background-color: #ffffc8;
        }

        .dynamic_highlight {
            background-color: #c8ffff;
        }
    `);

    let re = new RegExp(/^\s*(\d+\.\d+)\s+(\d+\.\d+)\s*([+-]?\d+\.\d+)/);

    // retr HTTP-GET parameter
    const getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };

    const retr_date = function retr_date() {
        try {
            return getUrlParameter('datum')
                   .split('-')
                   .map(d => parseInt(d));
        } catch (error) {
            var d = new Date();
            return [ d.getFullYear(), d.getMonth() +1, d.getDate() ];
        }
    };

    const REF_DATE = retr_date();

    const next_month = function next_month(today) {
        // today: [ year, month, day ]
        if (today[1] == 12) {
            return [ today[0] +1, 1, 1 ];
        } else {
            return [ today[0], today[1] +1, 1 ];
        }
    };

    const prev_month = function prev_month(today) {
        if (today[1] == 1) {
            return [ today[0] -1, 12, 1 ];
        } else {
            return [ today[0], today[1] -1, 1 ];
        }
    };


    const ts_to_hfrac = function ts_to_hfrac(ts) {
        return ( Number(ts.split(':')[0]) + Number(ts.split(':')[1]) / 60 );
    };

    const quarter_round = function quarter_round(n) {
        return ( Math.round( n * 4 ) / 4 );
    };


    /*
     *    CONTROL ELEMENTS
     */

    window.$('#appbar').css('position', 'fixed');

    // XXX FIXME Schrott, muss nach inhalt filtern
    var preexisting_nodes = window.$('#appbar:nth-child(1)').find('a');

    // 0: "Pflegealltag"
    // 1: "Messenger" (inside <div>)
    // 2: "Abmelden"
    // (Stand: 2020-12-01)

    window.$(preexisting_nodes[0]).remove();

    window.$(preexisting_nodes[1]).empty();
    window.$(preexisting_nodes[1]).append("&#x1f5ea;");

    window.$(preexisting_nodes[2]).remove();

    window.$('#appbar').prepend("<a class='spacer'></a>");
    window.$('#appbar').prepend("<a id='mon_fwd'>MONAT VOR &#9654;&#9654&#9646;</a>");
    window.$('#appbar').prepend("<a id='mon_bck'>&#9646;&#9664;&#9664; MONAT ZURÜCK</a>");
    window.$('#appbar').prepend("<a class='spacer'></a>");

    var n_month = next_month(REF_DATE);
    var p_month = prev_month(REF_DATE);

    let next_month_str = window.location.origin + window.location.pathname
                        + "?details=" + (getUrlParameter("details") || "nein")
                        + "&datum=" + n_month[0] + "-" + n_month[1] + "-" + n_month[2];

    let prev_month_str = window.location.origin + window.location.pathname
                        + "?details=" + (getUrlParameter("details") || "nein")
                        + "&datum=" + p_month[0] + "-" + p_month[1] + "-" + p_month[2];

    window.$('#mon_fwd').attr("href", next_month_str);
    window.$('#mon_bck').attr("href", prev_month_str);

    window.$('#appbar').prepend("<a class='spacer'></a>");
    window.$('#appbar').prepend("<a class='label'><b>" + getUrlParameter('details').toUpperCase() + "</b></a>");

    window.$('#appbar').prepend("<a class='spacer'></a>");
    window.$('#appbar').prepend("<a class='label'>" + REF_DATE[0] + '-' + (REF_DATE[1] <10 ? "0" + REF_DATE[1] : REF_DATE[1]) + "-" + (REF_DATE[2] <10 ? "0" + REF_DATE[2] : REF_DATE[2]) + "</a>");

    // stop pull-down menu animation
    window.$(".dropdown, .dropdown div, .dropdown li, .dropdown div::after").css("transition", "none");


    /*
     *    DAY DATE ROW
     */

    let appbar_height = window.$("#appbar")[0].offsetHeight;
    window.$("table.arbeitszeiten tr").first().css('position', 'sticky')
                                              .css('position', '-webkit-sticky')
                                              .css('top', appbar_height + 'px');

    window.$("table.arbeitszeiten th.tag").css('border-left-color', '#c1c2c3')
                                          .css('border-right-color', '#c1c2c3');

    window.$("table.arbeitszeiten th.tag").first().prev().css('border-right', '1px solid #c1c2c3');

    /*
     *    DE-HIGHLIGHT 0h-DAYS
     */

    window.$('td.tag').not('.leer').not('.summe').each( function(i, e) {
        let str = window.$(e)
                    .contents()
                    .filter( function() { return [ "#text", "EM" ].includes(this.nodeName) } );

        let figs = str.map( function() {
            if (this.nodeName == "EM") {
                return window.$(this).contents();
            } else {
                return window.$(this);
            }
        });

        if (Number(figs[1].text().replace(' ','')) == 0) {
            window.$(e).css('color', '#eee');
            window.$(e).find('em').css('color', '#fee');
        }
    });


    /*
     *    DE-RETARDATE TIME DISPLAY
     */

    window.$('td.summe').not('.tag').each( function(i, e) {
        let str = window.$(e)
                    .contents()
                    .filter( function() { return [ "#text", "EM" ].includes(this.nodeName) } );

        let figs = str.map( function() {
            if (this.nodeName == "EM") {
                return window.$(this).contents();
            } else {
                return window.$(this);
            }
        });

        // there are some "summe" cells, which aren't labeled empty even when empty...
        if (figs.length === 0) {
            return;
        }

        let t = figs[1].text().replace(' ','').replace('.',':');

        window.$(e).empty();
        window.$(e).append( t + '<br>' );
        window.$(e).append( '<b>' + quarter_round(ts_to_hfrac(t)) + '</b>');

/*
        if (Number(figs[1].text().replace(' ','')) == 0) {
            window.$(e).css('color', '#ddd');
            window.$(e).find('em').css('color', '#fdd');
        }
*/
    });


    /*
     *    HIGHLIGHT
     */

    var rows = window.$('table.arbeitszeiten').find('tr').slice(3);
    window.$(rows).click( function() {
        if(window.$(this).hasClass('static_highlight')) {
            window.$('.static_highlight').removeClass('static_highlight');
        } else {
            window.$('.static_highlight').removeClass('static_highlight');
            window.$(this).addClass('static_highlight');
        }
    });
    window.$(rows).hover( function() {
            window.$(this).addClass('dynamic_highlight');
        },
        function() {
            window.$(this).removeClass('dynamic_highlight');
        }
    );


    /*
     *    WASD CONTROLS
     */

    // q = 113 ; w = 119 ; e = 101 ; a = 97 ; s = 115 ; d = 100

    let data_height = 57;

    // keep controls from breaking, even when table is empty
    if (window.$("td.name").length > 0) {
        data_height = window.$("td.name").first()[0].offsetHeight;
    }

    console.log(data_height);

    window.$(document).on('keypress', function(e) {
        if (e.keyCode === 113) { // q
            console.log("MONAT ZURÜCK --> " + prev_month_str);
            window.location = prev_month_str;
            window.$(document).off('keypress');
            window.$("#mon_bck").css('background-color', '#c00000');
        } else if (e.keyCode === 101) { // e
            console.log("MONAT VOR --> " + next_month_str);
            window.location = next_month_str;
            window.$(document).off('keypress');
            window.$("#mon_fwd").css('background-color', '#c00000');
        } else if (e.keyCode === 97) { // a
//            console.log("TAG ZURÜCK --> " + yesterday_str);
//            window.location = yesterday_str;
//            window.$(document).off('keypress');
//            window.$("#day_bck").css('background-color', '#c00000');
        } else if (e.keyCode === 100) { // d
//            console.log("TAG VOR --> " + tomorrow_str);
//            window.location = tomorrow_str;
//            window.$(document).off('keypress');
//            window.$("#day_fwd").css('background-color', '#c00000');
        } else if (e.keyCode === 119) { // w
            window.$("html, body").animate({scrollTop: '-=' + data_height + 'px'}, {duration: 25, easing: "linear"});
        } else if (e.keyCode === 115) { // s
            window.$("html, body").animate({scrollTop: '+=' + data_height + 'px'}, {duration: 25, easing: "linear"});
        }
    });
})();
