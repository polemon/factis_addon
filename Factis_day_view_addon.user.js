// ==UserScript==
// @name         Factis_day_view_addon
// @namespace    http://tampermonkey.net/
// @version      0.4
// @description  add functions and enahnce view of Factis saas
// @author       Szymon 'polemon' Bereziak
// @match        <ENDPOINT>/mitarbeiter*
// @grant        GM_addStyle
// @require      https://code.jquery.com/jquery-3.6.0.min.js
// ==/UserScript==
// Last change   2021-11-05

(function() {
    'use strict';

    const TIME_R = [ 0.00, 6.00, 14.00, 20.00, 23.00, Infinity ];
    const DISREGARD = [ "Pause" ];

    GM_addStyle(`
        .spacer:hover, .label:hover {
            background-color: inherit !important;
        }

        .spacer, .label {
            border-left: 0 !important;
            font-size: 18px !important;
            vertical-align: top;
        }

        #day_fwd, #mon_fwd {
            background-color: rgb(32 167 136);
            border-right: 1px solid #1a71a0;
        }

        #day_fwd:hover, #mon_fwd:hover {
            background-color: rgb(16 95 77) !important;
        }

        .quicksums {
            margin: 3em auto;
            border-spacing: 0px;
            border-collapse: collapse;
            font-size: 12pt;
        }

        .quicksums td {
            border: 1px solid #666;
            border-top: 0; border-bottom: 0;
            padding: 0.3em 1em;
            width: 6em; text-align: center;
        }

        .quicksums > tr:nth-child(even) > td { background-color: #eee; }
        .quicksums > tr > td:last-child { background-color: transparent; }

        .quicksums .td_label {
            border: 0;
            text-align: right;
            width: 8em;
            font-size: 10pt;
            font-weight: bold;
        }

        #jobsums small {
            color: #888;
            font-size: 10px;
            vertical-align: 16%;
            margin-left: -3.25em;
        }

        .desc {
            margin-top: -2em;
            margin-bottom: 2em;
            text-align: center;
        }
    `);

    // retr HTTP-GET parameter
    const getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };

    const retr_date = function retr_date() {
        try {
            return getUrlParameter('datum')
                   .split('-')
                   .map(d => parseInt(d));
        } catch (error) {
            var d = new Date();
            return [ d.getFullYear(), d.getMonth() +1, d.getDate() ];
        }
    };

    const REF_DATE = retr_date();

    const MONTH_L = [ undefined, 31, ( REF_DATE[0] % 400 == 0 || ( REF_DATE[0] % 4 == 0 && REF_DATE[0] % 100 != 0 ) ) ? 29 : 28 , 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

    const tomorrow_date = function tomorrow_date(today) {
        // today: [ year, month, day ]
        if (today[2] == MONTH_L[ today[1] ] ) {
            if (today[1] == 12) {
                return [ today[0] +1, 1, 1 ];
            } else {
                return [ today[0], today[1] +1, 1 ];
            }
        } else {
            return [ today[0], today[1], today[2] +1];
        }
    };

    const yesterday_date = function yesterday_date(today) {
        // today: [ year, month, day ]
        if (today[2] == 1) {
            if (today[1] == 1) {
                return [ today[0] -1, 12, MONTH_L[12] ];
            } else {
                return [ today[0], today[1] -1, MONTH_L[today[1] -1] ];
            }
        } else {
            return [ today[0], today[1], today[2] -1];
        }
    };

    const next_month = function next_month(today) {
        // today: [ year, month, day ]
        if (today[1] == 12) {
            return [ today[0] +1, 1, 1 ];
        } else {
            return [ today[0], today[1] +1, 1 ];
        }
    };

    const prev_month = function prev_month(today) {
        if (today[1] == 1) {
            return [ today[0] -1, 12, 1 ];
        } else {
            return [ today[0], today[1] -1, 1 ];
        }
    };

    const ts_to_hfrac = function ts_to_hfrac(ts) {
        return ( Number(ts.split(':')[0]) + Number(ts.split(':')[1]) / 60 );
    };

    const quarter_round = function quarter_round(n) {
        return ( Math.round( n * 4 ) / 4 );
    };

    const intersection = function intersection(range_l, range_r, interval_l, interval_r) {
        if ( (range_l > interval_r) || (range_r < interval_l) ) {
            return 0; // no intersection
        } else {
            let l = Math.max(range_l, interval_l);
            let r = Math.min(range_r, interval_r);
            return (r - l);
        }
    };

    const overlap = function overlap(ranges, interval) {
        let results = [];

        for (let i = 0; i < ranges.length -1; i++) {
            results.push(intersection(ranges[i], ranges[i+1], interval[0], interval[1]));
        }

        return results;
    };

    const section_conv = function section_conv(ranges, day) {
        let total = [];
        let count = [];
        ranges.forEach( (e) => { total.push(0); count.push(0); } );

        day.forEach( (e) => {
            e.forEach( (o) => {
                //console.log(o.name);
                let overlaps = overlap(TIME_R, [ o.start, o.end ]);
                overlaps.forEach( function(v, i) {
                    if (v > 0) { count[i]++ };

                    total[i] += v;
                });
            });
        });

        return total.map( (e, i) => [ count[i], e ] );
    };

    const time_slices = function time_slices(day, disregard) {
        let slices = [];

        day.map( function(e) {
            let begin = 0;
            let end = 0;

            // XXX FIXME muss optimiert werden, sieht aus wie Dreck
            e.forEach( function(o, i) {
                if (i == 0) {
                    if (disregard.includes( o.name )) {
                        begin = o.end;
                    } else {
                        begin = o.start;
                    }
                } else {
                    if (disregard.includes( o.name )) {
                        slices.push( { "start" : begin, "end" : end, "name" : "" } );
                        begin = o.end;
                    }
                }
                end = o.end;
            });

            slices.push( { "start" : begin, "end" : end, "name" : "" } );
        });

        return slices.filter( e => e.end - e.start > 0 );
    };


    /*
     *    CONTROL ELEMENTS
     */

    window.$('#appbar').css('position', 'fixed');

    var preexisting_nodes = window.$('#appbar:nth-child(1)').find('a');

    // 0: "Pflegealltag"
    // 1: "Messenger" (inside <div>)
    // 2: "Abmelden"
    // (Stand: 2020-12-01)

    window.$(preexisting_nodes[0]).remove();

    window.$(preexisting_nodes[1]).empty();
    window.$(preexisting_nodes[1]).append("&#x1f5ea;");

    window.$(preexisting_nodes[2]).remove();

    window.$('#appbar').prepend("<a class='spacer'></a>");
    window.$('#appbar').prepend("<a id='day_fwd'>TAG VOR &#9654;&#9654;</a>");
    window.$('#appbar').prepend("<a id='day_bck'>&#9664;&#9664; TAG ZURÜCK</a>");
    window.$('#appbar').prepend("<a class='spacer'></a>");
    window.$('#appbar').prepend("<a id='mon_fwd'>MONAT VOR &#9654;&#9654&#9646;</a>");
    window.$('#appbar').prepend("<a id='mon_bck'>&#9646;&#9664;&#9664; MONAT ZURÜCK</a>");
    window.$('#appbar').prepend("<a class='spacer'></a>");

    var tomorrow = tomorrow_date(REF_DATE);
    var yesterday = yesterday_date(REF_DATE);

    var n_month = next_month(REF_DATE);
    var p_month = prev_month(REF_DATE);

    let tomorrow_str = window.location.origin + window.location.pathname
                        + "?zeitraum=" + (getUrlParameter("zeitraum") || "")
                        + "&__auto_factis_refresh__=true&autoaktualisierung=ja&berteam=&details=" + (getUrlParameter("details") || "nein")
                        + "&mid=" + (getUrlParameter("mid") || "")
                        + "&datum=" + tomorrow[0] + "-" + tomorrow[1] + "-" + tomorrow[2];

    let yesterday_str = window.location.origin + window.location.pathname
                        + "?zeitraum=" + (getUrlParameter("zeitraum") || "")
                        + "&__auto_factis_refresh__=true&autoaktualisierung=ja&berteam=&details=" + (getUrlParameter("details") || "nein")
                        + "&mid=" + (getUrlParameter("mid") || "")
                        + "&datum=" + yesterday[0] + "-" + yesterday[1] + "-" + yesterday[2];

    let next_month_str = window.location.origin + window.location.pathname
                        + "?zeitraum=" + (getUrlParameter("zeitraum") || "")
                        + "&__auto_factis_refresh__=true&autoaktualisierung=ja&berteam=&details=" + (getUrlParameter("details") || "nein")
                        + "&mid=" + (getUrlParameter("mid") || "")
                        + "&datum=" + n_month[0] + "-" + n_month[1] + "-" + n_month[2];

    let prev_month_str = window.location.origin + window.location.pathname
                        + "?zeitraum=" + (getUrlParameter("zeitraum") || "")
                        + "&__auto_factis_refresh__=true&autoaktualisierung=ja&berteam=&details=" + (getUrlParameter("details") || "nein")
                        + "&mid=" + (getUrlParameter("mid") || "")
                        + "&datum=" + p_month[0] + "-" + p_month[1] + "-" + p_month[2];

    window.$('#day_fwd').attr("href", tomorrow_str);
    window.$('#day_bck').attr("href", yesterday_str);

    window.$('#mon_fwd').attr("href", next_month_str);
    window.$('#mon_bck').attr("href", prev_month_str);

    const e_name = window.$('.beschreibung > .beschreibung_text:contains("Mitarbeiter:")').parent().find('.selected')[0].innerText;
    window.$('#appbar').prepend("<a class='label'>" + e_name + "</a>");
    window.$('#appbar').prepend("<a class='spacer'></a>");
    window.$('#appbar').prepend("<a class='label'>" + REF_DATE[0] + '-' + (REF_DATE[1] <10 ? "0" + REF_DATE[1] : REF_DATE[1]) + "-" + (REF_DATE[2] <10 ? "0" + REF_DATE[2] : REF_DATE[2]) + "</a>");


    /*
     *    TIMESTAMP
     */

    window.$('#topSelectForm > div.headerwrap').append('<span class="beschreibung_text" id="__TS__">__TS__</span>');
    window.$('#__TS__').css('float', 'right');
    window.$('#__TS__').css('margin-right', '1em');

    window.$('#__TS__').empty();
    let d = new Date();
    window.$('#__TS__').append(
        d.getFullYear() + '-'
        + ( (d.getMonth() +1) < 10 ? ('0' + (d.getMonth() +1) ) : (d.getMonth() +1) ) + '-'
        + ( (d.getDate() < 10) ? ('0' + d.getDate()) : d.getDate() ) + ' '
        + ( (d.getHours() < 10) ? ('0' + d.getHours()) : d.getHours() ) + ':'
        + ( (d.getMinutes() < 10) ? ('0' + d.getMinutes()) : d.getMinutes() ) + ':'
        + ( (d.getSeconds() < 10) ? ('0' + d.getSeconds()) : d.getSeconds() ) + ''
    );

    window.$(".dropdown, .dropdown div, .dropdown li, .dropdown div::after").css("transition", "none");


    /*
     *    QUICKSUMS TABLE
     */

    /*
     *    times table
     */

    window.$('<table class="quicksums" id="times_table"></table>').insertBefore('.zeituebersicht');
    window.$('#times_table').append('<tr id="sumhead"></tr>');
    window.$('#times_table').append('<tr id="sums"></tr>');
    window.$('#times_table').append('<tr id="ranges"></tr>');

    window.$('#sumhead').append("<td class='td_label'></td>");
    window.$('#sumhead').append("<td>06:00 - 14:00</td>");
    window.$('#sumhead').append("<td>14:00 - 20:00</td>");
    window.$('#sumhead').append("<td>20:00 - 23:00</td>");
    window.$('#sumhead').append("<td>23:00 - 06:00</td>");
    window.$('#sumhead').append("<td>Σ</td>");
    window.$('#sumhead').append("<td class='td_label'></td>");

    window.$('#sums').append("<td class='td_label' >Arbeitszeit:</td>");
    window.$('#sums').append("<td id='regular' >--.-- h</td>");
    window.$('#sums').append("<td id='a_noon'  >--.-- h</td>");
    window.$('#sums').append("<td id='evening' >--.-- h</td>");
    window.$('#sums').append("<td id='night'   >--.-- h</td>");
    window.$('#sums').append("<td id='total'   >--.-- h</td>");
    window.$('#sums').append("<td class='td_label' ></td>");

    window.$('#ranges').append("<td class='td_label' >inkl. Wegezeit:</td>");
    window.$('#ranges').append("<td id='r_regular' >--.-- h</td>");
    window.$('#ranges').append("<td id='r_a_noon'  >--.-- h</td>");
    window.$('#ranges').append("<td id='r_evening' >--.-- h</td>");
    window.$('#ranges').append("<td id='r_night'   >--.-- h</td>");
    window.$('#ranges').append("<td id='r_total'   >--.-- h</td>");
    window.$('#ranges').append("<td class='td_label' ></td>");


    /*
     *    jobs table
     */

    window.$('<table class="quicksums" id="jobs_table"></table>').insertBefore('.zeituebersicht');
    window.$('#jobs_table').append('<tr id="jobsumhead"></tr>');
    window.$('#jobs_table').append('<tr id="jobsums"></tr>');

    window.$('#jobsumhead').append("<td class='td_label'></td>");
    window.$('#jobsumhead').append("<td>Besuche</td>");
    window.$('#jobsumhead').append("<td>Büro</td>");
    window.$('#jobsumhead').append("<td>Sonstige Zeit</td>");
    window.$('#jobsumhead').append("<td>Arzt / Apotheke</td>");
    window.$('#jobsumhead').append("<td>Σ</td>");
    window.$('#jobsumhead').append("<td class='td_label'></td>");

    window.$('#jobsums').append("<td class='td_label'>Einsätze:</td>");
    window.$('#jobsums').append("<td id='visits'   >--</td>");
    window.$('#jobsums').append("<td id='office'   >--</td>");
    window.$('#jobsums').append("<td id='misc'     >--</td>");
    window.$('#jobsums').append("<td id='doc'      >--</td>");
    window.$('#jobsums').append("<td id='j_total'  >--</td>");
    window.$('#jobsums').append("<td class='td_label'></td>");

    window.$('<p class="desc">Σ(Besuche + Sonstige Zeit + Arzt / Apotheke)</p>').insertBefore('.zeituebersicht');


    /*
     *    DAY OBJECT CONSTRUCT
     */

    // ach factis...
    const nodes_ts_start = window.$('.pdca > tbody > tr > td.head:nth-child(2), .pdca > tbody > tr > td.headfoot:nth-child(2)')
                            .not('.versaeumt')
                            .find('.time');
    const nodes_ts_end = window.$('.pdca > tbody > tr > td.foot:nth-child(2), .pdca > tbody > tr > td.headfoot:nth-child(2)')
                            .not('.versaeumt')
                            .find('.time');

    const nodes_ts_start_filtered = window.$(nodes_ts_start).filter( (i) => {
        return ( ![ "Dienstbeginn" ].includes( window.$(nodes_ts_start[i]).next().text() ) );
    });

    let nodes_ts_names = window.$(nodes_ts_start_filtered).next();

/*
    window.$(nodes_ts_start_filtered).css('background-color', '#faa');
    window.$(nodes_ts_start_filtered).each( function(i, e) { window.$(e).append(" <i><b>[" + i + "]</b></i>") } );

    window.$(nodes_ts_end).css('background-color', '#faf');
    window.$(nodes_ts_end).each( function(i, e) { window.$(e).append(" <i><b>[" + i + "]</b></i>") } );

    window.$(nodes_ts_names).css('background-color', '#afa');
    window.$(nodes_ts_names).each( function(i, e) { window.$(e).append(" <i><b>[ " + i + " ]</b><i>") } );
*/

    const day = []; // workday construct
    let stretch = [];
    let min_t = 0;

    window.$(nodes_ts_start_filtered).each( (i, e) => {
        let s_t = ts_to_hfrac(window.$(e).text());
        let e_t = ts_to_hfrac(window.$(nodes_ts_end[i]).text());
        let name = window.$(nodes_ts_names[i]).text();

        s_t = s_t < min_t ? s_t +24 : s_t;
        e_t = e_t < min_t ? e_t +24 : e_t;

        min_t = Math.max(min_t, s_t, e_t);

        if ( [ "Dienstende" ].includes(name) ) {
            day.push(stretch);
            stretch = [];
        } else {
            stretch.push( { "start" : s_t, "end" : e_t, "name" : name, "name_obj" : nodes_ts_names[i] } );
        }
    });

    // ongoing day
    if (stretch.length != 0) {
        if ( isNaN(stretch[stretch.length -1].end) ) {
            let d = new Date();
            stretch[ stretch.length -1 ].end = (d.getHours() + d.getMinutes()/60);
            console.log("ONGOING JOB");
        }

        day.push(stretch);
        console.log("ONGOING DAY");
    }

    //console.log(day);


    /*
     *    PROCESS ENTRY DATA
     */

    /*
     *    Disregarding travel time
     */

    let filtered_day = day.map( e => e.filter( o => !DISREGARD.includes(o.name) ));

    let timed_sections = section_conv(TIME_R, filtered_day)
                            .map( e => e[1] + Math.floor(e[0]/2)/60 );

    let timed_total = section_conv(TIME_R, filtered_day)
                            .map( e => e[1] )
                            .reduce( (v, i) => v + i );

    if (timed_sections[1] > 0) {
        window.$('#regular').empty();
        window.$('#regular').append(quarter_round(timed_sections[1]) + " h");
    }

    if (timed_sections[2] > 0) {
        window.$('#a_noon').empty();
        window.$('#a_noon').append(quarter_round(timed_sections[2]) + " h");
    }

    if (timed_sections[3] > 0) {
        window.$('#evening').empty();
        window.$('#evening').append(quarter_round(timed_sections[3]) + " h");
    }

    if (timed_sections[0] + timed_sections[4] > 0) {
        window.$('#night').empty();
        window.$('#night').append(quarter_round(timed_sections[0] + timed_sections[4]) + " h");
    }

    if (timed_total > 0) {
        window.$('#total').empty();
        window.$('#total').append(quarter_round(timed_total) + " h");
    }


    /*
     *    including travel time
     */

    const slices = time_slices(day, DISREGARD);

    timed_sections = section_conv(TIME_R, [ slices ])
                            .map( e => e[1] + Math.floor(e[0]/2)/60 );

    timed_total = section_conv(TIME_R, [ slices ])
                            .map( e => e[1] )
                            .reduce( (v, i) => v + i );

    if (timed_sections[1] > 0) {
        window.$('#r_regular').empty();
        window.$('#r_regular').append(quarter_round(timed_sections[1]) + " h");
    }

    if (timed_sections[2] > 0) {
        window.$('#r_a_noon').empty();
        window.$('#r_a_noon').append(quarter_round(timed_sections[2]) + " h");
    }

    if (timed_sections[3] > 0) {
        window.$('#r_evening').empty();
        window.$('#r_evening').append(quarter_round(timed_sections[3]) + " h");
    }

    if (timed_sections[0] + timed_sections[4] > 0) {
        window.$('#r_night').empty();
        window.$('#r_night').append(quarter_round(timed_sections[0] + timed_sections[4]) + " h");
    }

    // ... <-- spread operator in ES6 (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax)
    if (timed_total > 0) {
        window.$('#r_total').empty();
        window.$('#r_total').append(quarter_round(timed_total) + " h");
    }


    /*
     *    JOB / JOB TYPE COUNTING
     */

    const count_types = function count_types(regs, day) {
        let counts = [];
        regs.forEach( x => counts.push(0) );

        day.forEach( function(e) {
            e.forEach( function(o) {
                regs.forEach( function(re, i) {
                    if (o.name.match(re)) {
                        counts[i]++;
                    }
                });
            });
        });

        return counts;
    }

    const TYPE_REGEX = [
        new RegExp(/^(\d+)\.\s([^,]+),\s(\w+)/),    // Besuch
        new RegExp(/^Büro/i),                       // Büro
        new RegExp(/^Sonstige Zeit/i),              // Sonstige Zeit
        new RegExp(/^Arzt/i),                       // Arzt
        new RegExp(/^Apotheke/i)                    // Apotheke
    ];

    const TYPE_COLOR = [
        '#ddf',
        '#bfb',
        '#fda',
        '#faf',
        '#faf'
    ];

    let item_count = count_types(TYPE_REGEX, day);

    //    DETECT CONSECUTIVES

    let consecutive = [];
    TYPE_REGEX.forEach( e => consecutive.push(0) );

    day.forEach( function(e) {
        e.forEach( function(o, i) {
            if (e[i-1]) {
                TYPE_REGEX.forEach( function(r, j) {
                    let previous_match = e[i-1].name.match(r);
                    let current_match = o.name.match(r);

                    if (current_match && previous_match) {
                        if (previous_match[2] == current_match[2]) {
                            consecutive[j]++;
                            window.$(e[i-1].name_obj).css('background-color', TYPE_COLOR[j]);
                            window.$(o.name_obj).css('background-color', TYPE_COLOR[j]);
                            window.$(o.name_obj).append(' <b>[ CONS ]</b>');
                        }
                    }
                });
            }
        });
    });

    if (item_count[0] > 0) {
        window.$('#visits').empty();
        window.$('#visits').append( ( consecutive[0] ? ( "<small>" + item_count[0] + " - " + consecutive[0] + " =</small> " ) : "" ) + (item_count[0] - consecutive[0]) );
    }

    if (item_count[1] > 0) {
        window.$('#office').empty();
        window.$('#office').append( ( consecutive[1] ? ( "<small>" + item_count[1] + " - " + consecutive[1] + " =</small> " ) : "" ) + (item_count[1] - consecutive[1]) );
    }

    if (item_count[2] > 0) {
        window.$('#misc').empty();
        window.$('#misc').append( ( consecutive[2] ? ( "<small>" + item_count[2] + " - " + consecutive[2] + " =</small> " ) : "" ) + (item_count[2] - consecutive[2]) );
    }

    if ( (item_count[3] + item_count[4]) > 0 ) {
        window.$('#doc').empty();
        window.$('#doc').append(
            ( (consecutive[3] + consecutive[4]) ? ( "<small>(= " + (item_count[3] + item_count[4]) + " - " + (consecutive[3] + consecutive[4]) + " =</small> " ) : "" )
            + ( (item_count[3] + item_count[4]) - (consecutive[3] + consecutive[4]) ) );
    }

    let j_totals = [ (item_count[0] - consecutive[0]), (item_count[2] - consecutive[2]), ( (item_count[3] + item_count[4]) - (consecutive[3] + consecutive[4]) ) ];
    // ... <-- spread operator in ES6 (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax)
    if ( Math.max( ...item_count ) > 0 ) {
        window.$('#j_total').empty();
        window.$('#j_total').append(
            ( ((j_totals[1] + j_totals[2]) > 0) ? ( "<small>" + j_totals[0] + " + " ) : "" )
            + ( j_totals[1] > 0 ? j_totals[1] : "" )
            + ( ((j_totals[1] && j_totals[2]) > 0) ? " + " : "" )
            + ( j_totals[2] > 0 ? j_totals[2] : "" )
            + ( ((j_totals[1] + j_totals[2]) > 0) ? "=</small> " : "" )
            + j_totals.reduce( (v, i) => v + i )
        );
    }


    /*
     *    WASD CONTROLS
     */

    // q = 113 ; w = 119 ; e = 101 ; a = 97 ; s = 115 ; d = 100

    let data_height = window.$("#pageheader").first()[0].offsetHeight;

    window.$(document).on('keypress', function(e) {
        if (e.keyCode === 113) { // q
            console.log("MONAT ZURÜCK --> " + prev_month_str);
            window.location = prev_month_str;
            window.$(document).off('keypress');
            window.$("#mon_bck").css('background-color', '#c00000');
        } else if (e.keyCode === 101) { // e
            console.log("MONAT VOR --> " + next_month_str);
            window.location = next_month_str;
            window.$(document).off('keypress');
            window.$("#mon_fwd").css('background-color', '#c00000');
        } else if (e.keyCode === 97) { // a
            console.log("TAG ZURÜCK --> " + yesterday_str);
            window.location = yesterday_str;
            window.$(document).off('keypress');
            window.$("#day_bck").css('background-color', '#c00000');
        } else if (e.keyCode === 100) { // d
            console.log("TAG VOR --> " + tomorrow_str);
            window.location = tomorrow_str;
            window.$(document).off('keypress');
            window.$("#day_fwd").css('background-color', '#c00000');
        } else if (e.keyCode === 119) { // w
            window.$("html, body").animate({scrollTop: '-=' + data_height + 'px'}, {duration: 25, easing: "linear"});
        } else if (e.keyCode === 115) { // s
            window.$("html, body").animate({scrollTop: '+=' + data_height + 'px'}, {duration: 25, easing: "linear"});
        }
    });

    // intended for JSON export / auto payroll
    const day_dataset = {};
    day_dataset.employee = { "name" : e_name.split(', ')[0], "1st_name" : e_name.split(', ')[1] };
    day_dataset.date = { "year" : REF_DATE[0], "month" : REF_DATE[1], "day" : REF_DATE[2] };
    day_dataset.record = day.map( e => e.map( o => { delete o.name_obj; return o } ) );
    //console.log(JSON.stringify(day_dataset));
})();
